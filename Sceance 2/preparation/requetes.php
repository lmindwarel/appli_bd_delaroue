<?php

//1.les photos de l'annonce 22,
Photo::where('id_annonce', '=', 22)->get();

//2.les photos de l'annonce 22 dont la taille en octets est > 100000
Photo::where('id_annonce', '=', 22)->where('taille_octet', '>', 100000)->get();

//3.les annonces possédant plus de 3 photos
Annonce::select('annonce.id_annonce')->join('photo', 'photo.id_annonce', '=', 'annonce.id_annonce')->groupBy('annonce.id_annonce')->having(DB::raw('COUNT(`id_photo`)'), '>', 3)->get();

//4.les annonces possédant des photos dont la taille est > 100000
Annonce::select('annonce.id_annonce')->join('photo', 'photo.id_annonce', '=', 'annonce.id_annonce')->groupBy('annonce.id_annonce')->having('taille_octet', '>', 100000)->get();

//ajouter une photo à l'annonce 22
$photo = new Photo('ma-photo.jpg', '01/02/2017', 134323423);
$photo.save();
