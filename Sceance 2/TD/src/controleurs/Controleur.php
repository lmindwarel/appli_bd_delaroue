<?php

namespace appli_bd\controleurs;

use \appli_bd\modeles\Company;
use \appli_bd\modeles\Game;
use \appli_bd\modeles\Character;
use \appli_bd\modeles\Platform;
use \appli_bd\vues\Vue;

class Controleur{

  public function requete(){
    /*perso du jeu 12342*/
    $res = "<div class='div_requete'><h3>afficher (name, deck) les personnages du jeu 12342</h3>";
    $res .= "<ul>";
    $persos = Game::find(12342)->characters()->get();
    foreach ($persos as $perso) {
      $res .= "<li>Name: $perso->name, Deck: $perso->deck</li>";
    }
    $res .= "</ul></div>";

    /*personnages des jeux dont le nom (du jeu) débute par 'Mario'*/
    $res .= "<div class='div_requete'><h3>personnages des jeux dont le nom débute par 'Mario'</h3>";
    $res .= "<ul>";
    $games = Game::where('name', 'LIKE', '%Mario%')->get();
    foreach ($games as $game){
      $game->characters()->get();
      foreach ($persos as $perso){
        $res .= "<li>Name: $perso->name, Deck: $perso->deck</li>";
      }
    }
    $res .= "</ul></div>";

    /*Jeu developpé par une compagnie dont le nom contient Sony*/
    $res .= "<div class='div_requete'><h3>Jeu developpé par une compagnie dont le nom contient Sony</h3>";
    $res .= "<ul>";
    $compagnies = Company::where('name', 'LIKE', '%sony%')->get();
    foreach ($compagnies as $compagny){
      $games = $compagny->games()->get();
      foreach ($games as $game){
        $res .= "<li>$game->name</li>";
      }
    }
    $res .= "</ul></div>";

    /*rating initial (indiquer le rating board) des jeux dont le nom contient Mario*/
    $res .= "<div class='div_requete'><h3>rating initial (indiquer le rating board) des jeux dont le nom contient Mario</h3>";
    $res .= "<ul>";
    $games = Game::where('name', 'LIKE', '%Mario%')->get();
    foreach ($games as $game){
      $rating_board = $game->rating()->get()->initialRating()->get();
      $res .= "<li>Rating initial: $rating_board</li>";
    }
    $res .= "</ul></div>";
  }
}
