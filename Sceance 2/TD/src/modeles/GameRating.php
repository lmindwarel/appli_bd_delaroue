<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class GameRating extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'game_rating';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function initialRating(){
    return $this->belongsTo('appli_bd\modeles\RatingBoard', 'game_rating', 'rating_board_id');
  }

  public function games(){
    return $this->belongsToMany('appli_bd\modeles\Game', 'game2rating', 'rating_id', 'game_id');
  }
}
