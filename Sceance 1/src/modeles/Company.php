<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Company extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'company';
  protected $primaryKey = 'id';
  public $timestamps = false;
}
