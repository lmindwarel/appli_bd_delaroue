<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Game extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'game';
  protected $primaryKey = 'id';
  public $timestamps = false;
}
