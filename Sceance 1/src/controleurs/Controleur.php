<?php

namespace appli_bd\controleurs;

use \appli_bd\modeles\Company;
use \appli_bd\modeles\Game;
use \appli_bd\modeles\Character;
use \appli_bd\modeles\Platform;
use \appli_bd\vues\Vue;

class Controleur{

  public function requete(){
    /*Mario*/
    $res = "<div class='div_requete'><h3>Jeu dont le nom contient 'Mario'</h3>";
    $res .= "<ul>";
    $jeux = Game::where('name', 'LIKE', '%Mario%')->get();
    foreach ($jeux as $jeu) {
      $res .= "<li>$jeu->name</li>";
    }
    $res .= "</ul></div>";

    /*Compagnies au Japon*/
    $res .= "<div class='div_requete'><h3>Compagnies au Japon</h3>";
    $res .= "<ul>";
    $compagnies = Company::where('location_country', 'LIKE', 'Japan')->get();
    foreach ($compagnies as $compagnie) {
      $res .= "<li>$compagnie->name</li>";
    }
    $res .= "</ul></div>";

    /*Compagnies au Japon*/
    $res .= "<div class='div_requete'><h3>Platforme dont la base est >= 10 000 000</h3>";
    $res .= "<ul>";
    $platformes = Platform::where('install_base', '>=', 10000000)->get();
    foreach ($platformes as $platforme) {
      $res .= "<li>$platforme->name</li>";
    }
    $res .= "</ul></div>";

    /*442 jeux à partir du 21173ème*/
    $res .= "<div class='div_requete'><h3>442 jeux à partir du 21173ème</h3>";
    $res .= "<ul>";
    $jeux = Game::skip(21172)->take(442)->get();
    foreach ($jeux as $jeu) {
      $res .= "<li>$jeu->name</li>";
    }
    $res .= "</ul></div>";

    /*Pagination*/
    $res .= "<div class='div_requete'><h3>Pagination</h3>";
    $res .= "<ul>";
    $jeux = Game::get();
    $i = 0;
    $page = 1;
    foreach ($jeux as $jeu) {
      $res .= "<li>$page Jeu: $jeu->name | Deck: $jeu->deck</li>";
      if($i == 500){
        $page++;
        $i = 0;
      }
      $i ++;
    }
    $res .= "</ul></div>";

    return $res;
  }
}
