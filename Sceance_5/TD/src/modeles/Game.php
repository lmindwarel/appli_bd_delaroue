<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Game extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'game';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function characters(){
    return $this->belongsToMany('appli_bd\modeles\Character', 'game2character', 'game_id', 'character_id');
  }

  public function comments(){
    return $this->hasMany('appli_bd\modeles\Comment', 'id_game');
  }

  public function ratings(){
    return $this->belongsToMany('appli_bd\modeles\GameRating', 'game2rating', 'game_id', 'rating_id');
  }

  public function platforms(){
    return $this->belongsToMany('appli_bd\modeles\Platform', 'game2platform', 'game_id', 'platform_id');
  }
}
