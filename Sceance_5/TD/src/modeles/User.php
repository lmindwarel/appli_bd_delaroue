<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class User extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'user';
  protected $primaryKey = 'email';
  public $timestamps = true;

  public function comments(){
    return $this->hasMany('appli_bd\modeles\Comment', 'email', 'email');
  }
}
