<?php

namespace appli_bd\controleurs;

use \appli_bd\modeles\Company;
use \appli_bd\modeles\Game;
use \appli_bd\modeles\Character;
use \appli_bd\modeles\Platform;
use \appli_bd\vues\Vue;
use \appli_bd\modeles\User;
use \appli_bd\modeles\Comment;
use \Illuminate\Database\Capsule\Manager as DB;

class Controleur{

  public function getGame($id){
    $game_query = Game::where('id', '=', $id)->first();
    $game = array('id' => $game_query->id, 'name' => $game_query->name, 'alias' => $game_query->alias, 'deck' => $game_query->deck, 'description' => $game_query->description, 'original_release_date' => $game_query->original_release_date);
    $links = ['comments' => array('href' => "/api/games/".$id."/comments"), 'characters' => array('href' => "/api/games/".$id."/characters")];
    $platforms = Game::find($id)->platforms()->get();
    $res = array('game' => $game, 'links' => $links, 'platforms' => $platforms);
    return json_encode($res);
  }

  public function getGames($page = 0){
    $games_query = Game::skip($page * 200)->take(200)->get();
    foreach ($games_query as $game_query) {
      $game = array('id' => $game_query->id, 'name' => $game_query->name, 'alias' => $game_query->alias, 'deck' => $game_query->deck);
      $links = array('self' => array('href' => "/api/games/".$game_query->id));
      $games[] = array('game' => $game, 'links' => $links);
    }
    $links = ['prev' => array('href' => "/api/games?page=".($page - 1)), 'characters' => array('href' =>  "/api/games?page=".($page + 1))];
    $res = array('games' => $games, 'links' => $links);
    return json_encode($res);
  }

  public function getGameComments($id){
    $comments_query = Game::find($id)->comments()->get();
    foreach ($comments_query as $comment) {
      $comment =  array('id' => $comment->id, 'titre' => $comment->titre, 'contenu' => $comment->contenu, 'date_creation' => $comment->date_creation);
      $comments[] = array('comment' => $comment);
    }
    $res = array('comment' => $comments);
    return json_encode($comments);
  }

  public function getGameCharacters($id){
    $characters_query = Game::find($id)->characters()->get();
    foreach ($characters_query as $character) {
      $links = array('self' => array('href' => "/api/character/".$character->id));
      $character =  array('id' => $character->id, 'name' => $character->name, 'links' => $links);
      $characters[] = array('character' => $character, 'links' => $links);
    }
    $res = array('characters' => $characters);
    return json_encode($res);
  }

  public function setGameComment($data){
    $comment_data = json_decode($data);
    $comment = new Comment();
    $comment->titre = $comment_data->titre;
    $comment->texte = $comment_data->texte;
    $comment->email = $comment_data->email;
    $comment->date_creation = date("Y-m-d");
    $comment->save();
    return $comment;
  }
}
