<?php

require_once 'vendor/autoload.php';

use \Illuminate\Database\Capsule\Manager as DB;
use \appli_bd\controleurs\Controleur;
use \Slim\Slim;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

$app = new Slim();
$config = parse_ini_file('conf.ini');
$DB = new DB();
$DB->addConnection($config);
$DB->setAsGlobal();
$DB->bootEloquent();

$app->response->headers->set('Content-Type', 'application/json');

$app->get("/", function () {
});

$app->get("/api/games/:id", function ($id) {
  $controleur = new Controleur();
  $res = $controleur->getGame($id);
  echo $res;
});

$app->get("/api/games", function () {
  $controleur = new Controleur();
  if(isset($_GET['page']))$res = $controleur->getGames($_GET['page']);
  else $res = $controleur->getGames();
  echo $res;
});

$app->get("/api/games/:id/comments", function ($id) {
  $controleur = new Controleur();
  $res = $controleur->getGameComments($id);
  echo $res;
});

$app->get("/api/games/:id/characters", function ($id) {
  $controleur = new Controleur();
  $res = $controleur->getGameCharacters($id);
  echo $res;
});

$app->post("/api/games/:id/comments", function ($request, $response, $id) {
  $controleur = new Controleur();
  $data = $_POST['comment_data'];
  try{
    $res = $controleur->setGameComment($data);
    $response->withHeader('Location', '/api/comments/'.$res->id);
    $response->withStatus(201);
    echo json_encode($res);

  }
  catch(Exception $e){
    echo 'erreur';
  }
});

$app->run();
