<?php

require_once 'vendor/autoload.php';

use \Illuminate\Database\Capsule\Manager as DB;
use \appli_bd\controleurs\Controleur;

$config = parse_ini_file('conf.ini');
$DB = new DB();
$DB->addConnection($config);
$DB->setAsGlobal();
$DB->bootEloquent();
DB::connection()->enableQueryLog();

$controleur = new Controleur();
$requetes = $controleur->requete();

echo <<<END
<!DOCTYPE html>
  <html lang="fr">
    <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <link rel="stylesheet" />
      <title>AppliBD</title>
    </head>
    <body>
      <header>
        <h1>Appli BD | Scéance 3</h1>
      </header>
      <h2>Requetes:</h2>
      $requetes
    </body>
    <style type="text/css">
.div_requete {
  vertical-align: top;
  display: inline-block;
  border-style: solid;
  width: 50%;
  margin: 0;
  box-sizing: border-box;
}
</style>
  </html>
END;
