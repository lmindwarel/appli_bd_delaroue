<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Character extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'character';
  protected $primaryKey = 'id';
  public $timestamps = false;

  public function game(){
    return $this->belongsTo('appli_bd\modeles\Game', 'game2character', 'character_id', 'game_id');
  }
}
