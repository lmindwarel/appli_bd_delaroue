<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Platform extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'platform';
  protected $primaryKey = 'id';
  public $timestamps = false;
}
