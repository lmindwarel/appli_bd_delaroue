<?php

namespace appli_bd\controleurs;

use \appli_bd\modeles\Company;
use \appli_bd\modeles\Game;
use \appli_bd\modeles\Character;
use \appli_bd\modeles\Platform;
use \appli_bd\vues\Vue;
use \Illuminate\Database\Capsule\Manager as DB;

class Controleur{

  public function requete(){
    /*Lister les jeux*/
    $res = '<h2>Temps execution pour la selection de tous jeux</h2>';
    $time_start = Controleur::microtime_float();
    $games = Game::get();
    $time_end = Controleur::microtime_float();
    $time = $time_end - $time_start;
    $res .= "Resultat: ".$time;
    $res .= '<h3>Log</h3>';
    $res .= Controleur::showLogs();

    /*lister les jeux dont le nom contient 'Mario'*/
    $res .= "<h2>Temps d'execution pour la selection des jeux dont le nom contient 'Mario'</h2>";
    $time_start = Controleur::microtime_float();
    $games = Game::where('name', 'LIKE', '%Mario%')->get();
    $time_end = Controleur::microtime_float();
    $time = $time_end - $time_start;
    $res .= "Resultat: ".$time;
    $res .= '<h3>Log</h3>';
    $res .= Controleur::showLogs();

    /*lister les jeux dont le nom debute 'Mario'*/
    $res .= "<h2>Temps d'execution pour la selection des jeux dont le nom debute par 'Mario'</h2>";
    $time_start = Controleur::microtime_float();
    $games = Game::where('name', 'LIKE', 'Mario%')->get();
    $time_end = Controleur::microtime_float();
    $time = $time_end - $time_start;
    $res .= "Resultat: ".$time;
    $res .= '<h3>Log</h3>';
    $res .= Controleur::showLogs();

    /*lister les jeux dont le nom debute 'Mario' et dont le rating initial contient '3+*/
    $res .= "<h2>Temps d'execution pour la selection des jeux dont le nom debute par 'Mario' et dont le rating initial contient '3+</h2>";
    $time_start = Controleur::microtime_float();
    $games = Game::where('name', 'LIKE', 'Mario%')->whereHas("ratings", function($query) {
      $query->where('name', 'like', '%3+%');
    })->get();
    $time_end = Controleur::microtime_float();
    $time = $time_end - $time_start;
    $res .= "Resultat: ".$time;
    $res .= '<h3>Log</h3>';
    $res .= Controleur::showLogs();

    return $res;
  }

  public static function showLogs(){
    $res = '';
    $i = 0;
    $logs = DB::getQueryLog();
    foreach ($logs as $log) {
      var_dump($log['bindings']);
      $i ++;
      $tmp = [];
      for($i = 0; $i < $log['bindings'].count(); $i++){
        $tmp[$i] = "?";
      }
      $res .= str_replace($tmp, $log['bindings'], $log["query"])."<br/>";
    }
    return $res;
  }

  /**
  * Fonction simple identique à celle en PHP 5 qui va suivre
  */
  public static function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
  }
}
