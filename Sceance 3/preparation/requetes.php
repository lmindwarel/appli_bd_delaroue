<?php

/**
* Fonction simple identique à celle en PHP 5 qui va suivre
*/
function microtime_float(){
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$time_start = microtime_float();

// Attend pendant un moment
usleep(100);

$time_end = microtime_float();
$time = $time_end - $time_start;

echo "Ne rien faire pendant $time secondes\n";

/*
Le principe de l'index est de pouvoir acceder plus rapidement aux données les plus utilisées
Lors d'une reauete mysql va en prioriter rechercher sur les indexs.

Les N+1 query s'explique par le fait qu'une requete sql est decoupee en plusieur morceau
Exemple: Select * from vehicule where couleur = 'rouge'
1 Select * from vehicule
1 for each select vehicule if couleur = 'rouge';

*/
