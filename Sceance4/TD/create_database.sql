CREATE TABLE `user` (
  `email` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `adresse` varchar(128) NOT NULL,
  `numero` varchar(128) NOT NULL,
  `date_naissance` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(128) NOT NULL,
  `contenu` text NOT NULL,
  `date_creation` date NOT NULL,
  `email` varchar(128) NOT NULL,
  `id_game` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`email`) REFERENCES user(`email`),
  FOREIGN KEY (`id_game`) REFERENCES game(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
