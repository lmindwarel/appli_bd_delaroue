<?php

namespace appli_bd\controleurs;

use \appli_bd\modeles\Company;
use \appli_bd\modeles\Game;
use \appli_bd\modeles\Character;
use \appli_bd\modeles\Platform;
use \appli_bd\vues\Vue;
use \appli_bd\modeles\User;
use \appli_bd\modeles\Comment;
use \Illuminate\Database\Capsule\Manager as DB;

class Controleur{

  public function requete($faker){

    $delete = Comment::where("email", 'like', "salutsalut@gmail.com")->delete();
    $delete = Comment::where("email", 'like', "guigui@gmail.com")->delete();
    $delete = User::where("email", 'like', "salutsalut@gmail.com")->delete();
    $delete = User::where("email", 'like', "guigui@gmail.com")->delete();

    $fabien = new User();
    $fabien->email = "salutsalut@gmail.com";
    $fabien->nom = "Dirate";
    $fabien->prenom = "Fabien";
    $fabien->adresse = "Avenue des chênes";
    $fabien->numero = "06 13 43 65 21";
    $fabien->date_naissance = date("Y-m-d", mktime(0, 0, 0, 6, 12, 1980));
    $fabien->save();

    $leonie = new User();
    $leonie->email = "guigui@gmail.com";
    $leonie->nom = "Arotine";
    $leonie->prenom = "Léonie";
    $leonie->adresse = "rue de l'appel";
    $leonie->numero = "07 34 66 23 10";
    $leonie->date_naissance = date("Y-m-d", mktime(0, 0, 0, 9, 2, 1998));
    $leonie->save();

    $fabien_comment_1 = new Comment();
    $fabien_comment_1->titre = "C'est null";
    $fabien_comment_1->contenu = "Vraiment trop null ce jeu, pas du tout aimé";
    $fabien_comment_1->date_creation = date("Y-m-d");
    $fabien_comment_1->email = "salutsalut@gmail.com";
    $fabien_comment_1->id_game = 12342;
    $fabien_comment_1->save();

    $leonie_comment_2 = new Comment();
    $leonie_comment_2->titre = "Non la vraiment";
    $leonie_comment_2->contenu = "J'en suis au niveau 3 et ca confirme ce que je disait avant c'est vraiment null";
    $leonie_comment_2->date_creation = date("Y-m-d");
    $leonie_comment_2->email = "salutsalut@gmail.com";
    $leonie_comment_2->id_game = 12342;
    $leonie_comment_2->save();

    $leonie_comment_1 = new Comment();
    $leonie_comment_1->titre = "Mouais";
    $leonie_comment_1->contenu = "Pas d'histoire, mauvais graphiques";
    $leonie_comment_1->date_creation = date("Y-m-d");
    $leonie_comment_1->email = "guigui@gmail.com";
    $leonie_comment_1->id_game = 12342;
    $leonie_comment_1->save();

    $leonie_comment_2 = new Comment();
    $leonie_comment_2->titre = "Vraiment mauvais tout du long";
    $leonie_comment_2->contenu = "Ce jeu n'a aucun interet";
    $leonie_comment_2->date_creation = date("Y-m-d");
    $leonie_comment_2->email = "guigui@gmail.com";
    $leonie_comment_2->id_game = 12342;
    $leonie_comment_2->save();

    //Génération des utilisateurs
    for($i = 0; $i < 25000; $i ++){
      $user = new User();
      $user->email = $faker->email;
      $user->nom = $faker->name;
      $user->prenom = $faker->firstName;
      $user->adresse = $faker->address;
      $user->numero = $faker->phoneNumber;
      $user->date_naissance = $faker->date; // DateTime('2008-04-25 08:37:17', 'UTC'));
      $user->save();
    }

    //Génération des utilisateurs
    for($i = 0; $i < 25000; $i ++){
      $comment = new Comment();
      $comment->titre = $faker->realText(19);
      $comment->contenu = "J'en suis au niveau 3 et ca confirme ce que je disait avant c'est vraiment null";
      $comment->date_creation = date("Y-m-d");
      $comment->email = "salutsalut@gmail.com";
      $comment->id_game = 12342;
      $comment->save();
    }
  }
}
