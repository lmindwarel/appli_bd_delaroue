<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class RatingBoard extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'rating_board';
  protected $primaryKey = 'id';
  public $timestamps = false;
}
