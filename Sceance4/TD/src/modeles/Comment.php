<?php

namespace appli_bd\modeles;

/**
 * Classe modélisant un joueur de la quizzbox
 */
class Comment extends \Illuminate\DataBase\Eloquent\Model{

  protected $table = 'comment';
  protected $primaryKey = 'id';
  public $timestamps = true;

  public function user(){
    return $this->belongsTo('appli_bd\modeles\User', 'email', 'email');
  }
}
